Package.describe({
  name: 'inspinia-scss',
  version: '0.1.4',
  summary: 'scss styles of inspinia theme',
  documentation: 'README.md',
});
// todo перенести загрузку бутстрапа сюдой
//
Npm.depends({
  // 'bootstrap-sass': '3.3.7',
  // 'text-spinners': '1.0.5',
});

Package.onUse(function (api) {
  api.use('fourseven:scss@4.5.4');
  api.addFiles([
    'stylesheets/_variables.scss',
    'stylesheets/_mixins.scss',
    'stylesheets/_badges_labels.scss',
    'stylesheets/_base.scss',
    'stylesheets/_buttons.scss',
    'stylesheets/_chat.scss',
    'stylesheets/_checkbox.scss',
    'stylesheets/_clockpicker.scss',
    'stylesheets/_colorpicker.scss',
    'stylesheets/_datatables.scss',
    'stylesheets/_datepicker.scss',
    'stylesheets/_elements.scss',
    'stylesheets/_fixes.scss',
    'stylesheets/_landing.scss',
    'stylesheets/_md-skin.scss',
    'stylesheets/_media.scss',
    'stylesheets/_metismenu.scss',
    'stylesheets/_navigation.scss',
    'stylesheets/_pages.scss',
    'stylesheets/_print.scss',
    'stylesheets/_rtl.scss',
    'stylesheets/_select2.scss',
    'stylesheets/_sidebar.scss',
    'stylesheets/_skins.scss',
    'stylesheets/_spinners.scss',
    'stylesheets/_tagsinput.scss',
    'stylesheets/_text-spinners.scss',
    'stylesheets/_top_navigation.scss',
    'stylesheets/_typeahead.scss',
    'stylesheets/_typography.scss',
    'stylesheets/style.scss',
  ], 'client', { isImport: true });
});
